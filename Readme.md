# Base image for development for our olcms/www

This is base docker image for our developement enviornment, we 
have quite a lot of debian dependencies, and it is faster to 
install them once, and then just download them. 

This has two branches: ``master`` that contains base image for 
developement enviornment and ``prod`` that has base image
for production enviornment. 

When you edit ``master``, please checkout ``prod`` and rebase on 
master.


