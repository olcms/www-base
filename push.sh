#!/usr/bin/env bash

if ! git diff-index --quiet --cached HEAD --; then
    echo "Please commit everything"
    exit 1
fi

git push
git checkout prod
git rebase master
git push -f
git checkout master
