#!/usr/bin/env bash

set -e

export DEBIAN_FRONTEND=noninteractive
NODEREPO="node_7.x"

DISTRO=jessie

apt-get install -yq apt-transport-https

cat /install-scripts/nodesource.gpg.key  | apt-key add -
echo "deb https://deb.nodesource.com/${NODEREPO} ${DISTRO} main" > /etc/apt/sources.list.d/nodesource.list
echo "deb-src https://deb.nodesource.com/${NODEREPO} ${DISTRO} main" >> /etc/apt/sources.list.d/nodesource.list

apt-get update
