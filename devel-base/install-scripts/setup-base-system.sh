#!/usr/bin/env bash

set -e

apt-get update

apt-get upgrade -yq

/install-scripts/install-gosu.sh
/install-scripts/update-sources-for-node.sh

# For minor developement tasks inside the container. Never regretted installing basic tools
apt-get install telnet vim nano less grep -yq

apt-get install gettext -yq

if test "$1" == "install-selenium"
then
    set -e
    # Required by chromedriver
    apt-get install libgconf2-4  -yq
    apt-get install -yq xvfb libasound2 libatk1.0-0 libcups2 libgtk-3-0 libnspr4 libx11-xcb1 libxcomposite1 libxcursor1 libxdamage1 fonts-liberation libappindicator1 libnss3 xdg-utils lsb-release

    curl https://ocs-pl.oktawave.com/v1/AUTH_385fff76-290b-43da-b2fc-96b1c08bce24/tools/google-chrome-60.deb >  /tmp/google-chrome-stable_current_amd64.deb
    sha256sum /tmp/google-chrome-stable_current_amd64.deb | grep --quiet 9a64c54d87724b58ea5cff53b05a48583e1c26b48108d9e5723ff69ff05a7871
    dpkg --install /tmp/google-chrome-stable_current_amd64.deb

    # Download chromedriver from oktavawe (I'd rather use our own infrasctructure)
    # Version here is 2.30 (latest released at the time of writing)
    curl https://ocs-pl.oktawave.com/v1/AUTH_385fff76-290b-43da-b2fc-96b1c08bce24/tools/chromedriver-2.31.xz | xzcat > /usr/local/bin/chromedriver
    sha256sum /usr/local/bin/chromedriver | grep --quiet 55d30a327a729e2e932a6f3485a236054aa3d2cbf2ea93eed7e74ad0b7dd8efa
    chmod +x /usr/local/bin/chromedriver

fi

# For spell-check
apt-get install libenchant-dev enchant -qy

# Typescript compiler
apt-get install nodejs -yq
npm install -g typescript@2.1.5

apt-get autoremove -y
apt-get clean
rm -rf /var/cache/*
rm -rf /var/tmp/*
rm -rf /tmp/*

rm -rf /root/.npm
rm -rf /root/.cache


